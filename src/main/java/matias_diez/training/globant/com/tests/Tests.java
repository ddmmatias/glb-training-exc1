package matias_diez.training.globant.com.tests;

import matias_diez.training.globant.com.pages.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by matias.diez on 9/24/15.
 */
public class Tests {

    WebDriver driver;

    @BeforeMethod
    public void before () {
        driver = new FirefoxDriver();
    }

    @Test
    public void openSite(){
        HomePage homepage = PageFactory.initElements(driver, HomePage.class);
        homepage.goToUrl(driver);
        String actualTitle = driver.getTitle();
        String expectedTitle = "Automation Training | Aprender a automatizar en un solo sitio";
        Assert.assertEquals(actualTitle, expectedTitle);
    }

    @Test
    public void searchTesting(){
        HomePage homepage = PageFactory.initElements(driver, HomePage.class);
        homepage.goToUrl(driver);
        homepage.search("testing");
        ResultsPage resultspage = PageFactory.initElements(driver, ResultsPage.class);
        Assert.assertEquals(resultspage.getSearchTitle().toLowerCase(), "testing");
    }

    @Test
    public void checkPostDate (){
        HomePage homepage = PageFactory.initElements(driver, HomePage.class);
        UtilsPage utilsPage = PageFactory.initElements(driver, UtilsPage.class);
        homepage.goToUrl(driver);
        String attDate = homepage.getPostDate();
        String viewDate = homepage.getViewDate();
        viewDate = utilsPage.parseDate(viewDate);
        Assert.assertEquals(attDate, viewDate);
    }


    @Test
    public void contactUsSendForm(){
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);
        homePage.goToUrl(driver);
        homePage.openContactUsPage();
        ContactUsPage contactUsPage = PageFactory.initElements(driver, ContactUsPage.class);
        contactUsPage.completeContactUsForm();
        contactUsPage.submitContactUsForm();
        ContactUsConfirmationPage contactUsConfirmationPage = PageFactory.initElements(driver, ContactUsConfirmationPage.class);
        contactUsConfirmationPage.checkConfirmationText();
        Assert.assertTrue(true);
    }

    @Test
    public void contactUsWithEmptyFields(){
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);
        homePage.goToUrl(driver);
        homePage.openContactUsPage();
        ContactUsPage contactUsPage = PageFactory.initElements(driver, ContactUsPage.class);
        contactUsPage.partialContactUsForm();
        contactUsPage.submitContactUsForm();
        contactUsPage.checkFailureText();
        Assert.assertTrue(true);
        contactUsPage.cleanContactUsForm();
        contactUsPage.completeContactUsForm();
        contactUsPage.submitContactUsForm();
        ContactUsConfirmationPage contactUsConfirmationPage = PageFactory.initElements(driver, ContactUsConfirmationPage.class);
        contactUsConfirmationPage.checkConfirmationText();
        Assert.assertTrue(true);
    }

    @Test
    public void checkPostsInCalendar() {
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);
        homePage.goToUrl(driver);
        int publishedDays = homePage.countPublishedDays(homePage.listCurrentMonthCalendar());
        homePage.previousMonthArchive();
        int previousPublishedDays = homePage.countPublishedDays(homePage.listCurrentMonthCalendar());
        homePage.accessFirstPublishedDay(homePage.listCurrentMonthCalendar());
        UtilsPage utilsPage = PageFactory.initElements(driver, UtilsPage.class);
        utilsPage.listOfAllPostsDisplayed();
    }


    @AfterMethod 
    public void after(){
        driver.quit();
    }

}
