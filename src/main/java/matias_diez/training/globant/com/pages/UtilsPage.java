package matias_diez.training.globant.com.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by matias.diez on 9/30/15.
 */
public class UtilsPage {

    @FindBy(id="content")
    private WebElement postList;

    public void listOfAllPostsDisplayed () {
        List<WebElement> posts = postList.findElements(By.className("entry-title"));
        int postCount = posts.size();
        System.out.println("The are currently " + postCount + " posts in this page");
        System.out.println("The titles are: ");
        for (WebElement element : posts) {
            System.out.println(element.findElement(By.tagName("a")).getText());
        }
    }

    public String parseDate(String parsedDate) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("MMMMM dd, yyyy");
            Date newDate = sdf.parse(parsedDate);
            sdf.applyPattern("yyyy-MM-dd");
            parsedDate = sdf.format(newDate);
        } catch (Exception e) {
            System.out.println("Error " + e.getMessage());
        }
        return parsedDate;
    }

    public void waitSeconds(int seconds){
        try {
            Thread.sleep(seconds * 1000);
        } catch (Exception e){}
    }

}
