package matias_diez.training.globant.com.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by matias.diez on 9/24/15.
 */
public class ResultsPage {
    @FindBy(xpath=".//h1[contains(@class,'page-title')]/span")
    private WebElement pageTitle;

    public String getSearchTitle(){
        return pageTitle.getText();
    }

    public boolean validateSearchResults (ResultsPage resultspage){

        String result = resultspage.getSearchTitle();

        if (result.contentEquals("testing")){
            return true;
        } else return false;
    }
}

