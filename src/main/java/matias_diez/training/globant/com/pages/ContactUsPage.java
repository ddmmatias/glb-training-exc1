package matias_diez.training.globant.com.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by matias.diez on 9/29/15.
 */
public class ContactUsPage {

    @FindBy(id="cntctfrm_contact_name")
    private WebElement name;

    @FindBy(id="cntctfrm_contact_email")
    private WebElement email;

    @FindBy(id="cntctfrm_contact_subject")
    private WebElement subject;

    @FindBy(id="cntctfrm_contact_message")
    private WebElement message;

    @FindBy(xpath="//*[@id=\"cntctfrm_contact_form\"]/div[last()]/input[4]")
    private WebElement buttonSubmit;

    @FindBy(xpath="//*[@id=\"cntctfrm_contact_form\"]/div[1]")
    private WebElement contactUsFailMessage;

    public void completeContactUsForm() {
        name.sendKeys("Matias");
        email.sendKeys("ddmmatias@gmail.com");
        subject.sendKeys("Consulta Generica");
        message.sendKeys("Estoy haciendo la capacitacion de automation y debo enviar este mensaje");
    }

    public void partialContactUsForm() {
        name.sendKeys("Matias");
        email.sendKeys("ddmmatias@gmail.com");
        subject.sendKeys("Consulta Generica");
    }


    public void submitContactUsForm(){
        buttonSubmit.click();
    }

    public boolean checkFailureText(){
        String failureText = contactUsFailMessage.getText();
        if (failureText.contentEquals("Please make corrections below and try again.")){
            return true;
        } else return false;
    }

    public void cleanContactUsForm(){
        name.clear();
        email.clear();
        subject.clear();
        message.clear();
    }

}
