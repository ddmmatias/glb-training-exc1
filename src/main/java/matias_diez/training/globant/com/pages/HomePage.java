package matias_diez.training.globant.com.pages;

import org.apache.commons.collections.bag.SynchronizedSortedBag;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.security.spec.KeySpec;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by matias.diez on 9/24/15.
 */
public class HomePage {

    static final String TAG_LINK = "a";

    @FindBy(id="s")
    private WebElement searchform;

    @FindBy(xpath="//*[@id=\"post-41\"]/header/div/a/time")
    private WebElement entryDate;

    @FindBy(xpath="//*[@id=\"access\"]/div[3]/ul/li[2]/a")
    private WebElement contactUsButton;

    @FindBy(xpath="//*[@id=\"wp-calendar\"]/tbody")
    private WebElement homePageCalendar;


    @FindBy(xpath="//*[@id=\"prev\"]/a")
    private WebElement previousMonth;


    public void goToUrl (WebDriver driver) {
        driver.get("http://10.28.148.127/wordpress");
    }

    public void search (String query) {
        searchform.sendKeys(query);
        searchform.sendKeys(Keys.ENTER);
    }

    public String getPostDate(){
        String date1 = entryDate.getAttribute("datetime");
        String[] parts = date1.split("T");
        return parts[0];
    }

    public String getViewDate(){
        return entryDate.getText();
    }

    public void openContactUsPage(){
        contactUsButton.click();
    }

    public List listCurrentMonthCalendar () {
        List<WebElement> daysOfCurrentMonth = homePageCalendar.findElements(By.tagName("td"));
        return daysOfCurrentMonth;
    }

    public void previousMonthArchive(){
        previousMonth.click();
    }

    public int countPublishedDays (List<WebElement> elements) {
        int dayCount = 0;
        for (WebElement element : elements) {
            try {
                WebElement value = element.findElement(By.tagName(TAG_LINK));
                if (value != null) {
                    dayCount++;
                }
            } catch (Exception e){ }
        }
        return dayCount;
    }

    public void accessFirstPublishedDay (List<WebElement> elements) {
        for (WebElement element : elements) {
            try {
                WebElement value = element.findElement(By.tagName(TAG_LINK));
                if (value != null) {
                    element.findElement(By.tagName(TAG_LINK)).click();
                    break;
                }
            } catch (Exception e){ }
        }
    }
}
