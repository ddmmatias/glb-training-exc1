package matias_diez.training.globant.com.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by matias.diez on 9/29/15.
 */
public class ContactUsConfirmationPage {

    @FindBy(id="cntctfrm_thanks")
    private WebElement contactUsConfirmationText;

    public boolean checkConfirmationText(){
        String confirmationText = contactUsConfirmationText.getText();
        if (confirmationText.contentEquals("Thank you for contacting us.")){
            return true;
        } else return false;
    }


}
